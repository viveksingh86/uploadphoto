namespace UploadPhoto.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DDLChangesforPohotoTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Photos", "Path", c => c.String(nullable: false));
            AddColumn("dbo.Photos", "FileType", c => c.Int(nullable: false));
            AddColumn("dbo.Photos", "CreatedDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Photos", "UploadedBy", c => c.Guid(nullable: false));
            AddColumn("dbo.Photos", "IsDeleted", c => c.Boolean(nullable: false));
            DropColumn("dbo.Photos", "FilePath");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Photos", "FilePath", c => c.String(nullable: false));
            DropColumn("dbo.Photos", "IsDeleted");
            DropColumn("dbo.Photos", "UploadedBy");
            DropColumn("dbo.Photos", "CreatedDate");
            DropColumn("dbo.Photos", "FileType");
            DropColumn("dbo.Photos", "Path");
        }
    }
}
