﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using UploadPhoto.Models;

namespace UploadPhoto.Controllers
{
    public class HomeController : Controller
    {
        private PhotoContext db = new PhotoContext();

        public ActionResult Index()
        {
            return View();
        }

        [AcceptVerbs(HttpVerbs.Post)]
        public ActionResult Index(HttpPostedFileBase uploadFile)
        {
            return View();
        }


        public void SaveImage(HttpPostedFileBase uploadFile)
        {
            if (ValidateFile(uploadFile))
            {
                Photo photo = new Photo();
                photo.EntityGuid = Guid.NewGuid();
                //photo.FileName = photo.EntityGuid + "_" + Path.GetFileName(uploadFile.FileName);
                photo.FileName = Path.GetFileNameWithoutExtension(uploadFile.FileName);
                photo.Path = "~/Images/main/";
                photo.CreatedDate = DateTime.Now;
                photo.FileType = 1;// Normal file not sure if this is required but open for change
                photo.IsDeleted = false;// 
                photo.IsUploaded = false;//  
                //string relativePath = "~/Images/main/" + photo.FileName;
                string relativePath = "~/Images/main/" + Path.GetFileName(uploadFile.FileName);
                string physicalPath = Server.MapPath(relativePath);

                uploadFile.SaveAs(physicalPath);

                //ConvertFileTo ThumbNail
                string filePath, fileSize;
                var section = ConfigurationManager.GetSection("ThumbNail") as NameValueCollection;
                filePath = section["path"];
                fileSize = section["size"];

                string thumbPath = Server.MapPath(filePath);
                ResizeAndSave(thumbPath, Path.GetFileNameWithoutExtension(photo.FileName), uploadFile.InputStream, Convert.ToInt32(fileSize), true);

                section = ConfigurationManager.GetSection("Resized") as NameValueCollection;
                filePath = section["path"];
                fileSize = section["size"];

                //ConvertFileTo ThumbNail
                string resized = Server.MapPath(filePath);
                ResizeAndSave(resized, Path.GetFileNameWithoutExtension(photo.FileName), uploadFile.InputStream, Convert.ToInt32(fileSize), true);

                db.Photos.Add(photo);
                db.SaveChanges();
               
            }
        }

        private bool ValidateFile(HttpPostedFileBase uploadFile)
        {
            if (uploadFile == null)
            {
                return false;
            }
            if (uploadFile.ContentLength > 1 * 1024 * 1024)
            {
                return false;
            }

            try
            {
                using (var img = Image.FromStream(uploadFile.InputStream))
                {
                    return img.RawFormat.Equals(ImageFormat.Jpeg) || img.RawFormat.Equals(ImageFormat.Gif) || img.RawFormat.Equals(ImageFormat.Png);
                }

            }
            catch { };

            return false;


        }

        private bool ValidateImage(Stream stream)
        {

            try
            {
                //Read an image from the stream...
                var i = Image.FromStream(stream);

                //Move the pointer back to the beginning of the stream
                stream.Seek(0, SeekOrigin.Begin);

                if (ImageFormat.Jpeg.Equals(i.RawFormat))
                return true;
                return ImageFormat.Png.Equals(i.RawFormat) || ImageFormat.Gif.Equals(i.RawFormat);
            }
            catch
            {
                return false;
            }
        }


        public static void ResizeAndSave(string savePath, string fileName, Stream imageBuffer, int maxSideSize, bool makeItSquare)
        {
            int newWidth;
            int newHeight;
            Image image = Image.FromStream(imageBuffer);
            int oldWidth = image.Width;
            int oldHeight = image.Height;
            Bitmap newImage;
            if (makeItSquare)
            {
                int smallerSide = oldWidth >= oldHeight ? oldHeight : oldWidth;
                double coeficient = maxSideSize / (double)smallerSide;
                newWidth = Convert.ToInt32(coeficient * oldWidth);
                newHeight = Convert.ToInt32(coeficient * oldHeight);
                Bitmap tempImage = new Bitmap(image, newWidth, newHeight);
                int cropX = (newWidth - maxSideSize) / 2;
                int cropY = (newHeight - maxSideSize) / 2;
                newImage = new Bitmap(maxSideSize, maxSideSize);
                Graphics tempGraphic = Graphics.FromImage(newImage);
                tempGraphic.SmoothingMode = SmoothingMode.AntiAlias;
                tempGraphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                tempGraphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                tempGraphic.DrawImage(tempImage, new Rectangle(0, 0, maxSideSize, maxSideSize), cropX, cropY, maxSideSize, maxSideSize, GraphicsUnit.Pixel);
            }
            else
            {
                int maxSide = oldWidth >= oldHeight ? oldWidth : oldHeight;

                if (maxSide > maxSideSize)
                {
                    double coeficient = maxSideSize / (double)maxSide;
                    newWidth = Convert.ToInt32(coeficient * oldWidth);
                    newHeight = Convert.ToInt32(coeficient * oldHeight);
                }
                else
                {
                    newWidth = oldWidth;
                    newHeight = oldHeight;
                }
                newImage = new Bitmap(image, newWidth, newHeight);
            }
            newImage.Save(savePath + fileName + "_" + maxSideSize + ".png", ImageFormat.Png);
            image.Dispose();
            newImage.Dispose();
        }  


        
        public ActionResult List()
        {
            return View(db.Photos.ToList());
        }

        
        public ActionResult UploadImage()
        {
            return View();
        }
    
        [HttpPost]
        public ActionResult UploadImage(HttpPostedFileBase file)
        {
            SaveImage(file);
            return View();
            return Content(file.FileName);
        }

     
    }
}
