﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace UploadPhoto.Models
{
    public class Photo
    {
        public int ID { get; set; }

        [Required] 
        public Guid EntityGuid { get; set; }

        [Required]
        public string FileName { get; set; }

        [Required]
        public string Path { get; set; }

        [Required]
        public int FileType { get; set; }

        [Required]
        public DateTime CreatedDate { get; set; }

        public Guid UploadedBy { get; set; }

        public bool IsUploaded { get; set; }

        public bool IsDeleted { get; set; }

        
    }


    public class PhotoContext : DbContext
    {
        public DbSet<Photo> Photos { get; set; }
    }

    
}